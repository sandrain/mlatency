#!/bin/bash

base=1024
count=10

for i in `seq 0 $count`; do
	size=$((base*(1<<i)))
	echo numactl -C 0 -m 0 ./mlatency $size
done

