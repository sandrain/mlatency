/**
 * use with numactl(8) to get the accurate results.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/time.h>

static int loop = 100;

int main(int argc, char **argv)
{
	int i;
	uint64_t memsize;
	char *src;
	char *dst;
	struct timeval before, after;

	if (argc != 2) {
		printf("Usage: %s <size in bytes>\n", argv[0]);
		return 0;
	}

	memsize = strtoull(argv[1], NULL, 0);

	src = calloc(2, memsize);
	if (!src) {
		perror("calloc");
		return -1;
	}
	dst = &src[memsize];

	for (i = 0; i < memsize; i++) {
		src[i] = (i % 26) + 'a';
		dst[i] = (i % 26) + 'A';
	}

	gettimeofday(&before, NULL);

	while (loop--)
		for (i = 0; i < memsize; i++)
			dst[i] = src[i];

	gettimeofday(&after, NULL);

	if (after.tv_usec < before.tv_usec) {
		after.tv_sec -= 1;
		after.tv_usec += 1000000;
	}

	printf("%llu %llu.%0llu\n", (unsigned long long) memsize,
		(unsigned long long) (after.tv_sec - before.tv_sec),
		(unsigned long long) (after.tv_usec - before.tv_usec));

	return 0;
}

